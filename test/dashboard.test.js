'use strict';
const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
should;

var mongoose = require('mongoose');

// Import server
var server = require('../app');

// Import Todo Model
var Dashboard = require('../models/dashboard');

const jsonwebtoken = require('jsonwebtoken');

var tmpDashID = 0;

jest.mock('jsonwebtoken');

jsonwebtoken.verify.mockReturnValue(
  {
    username: 'MyUser',
    groups: 'group1',
  },
);

// Clear database after all tests are done
afterAll(async() => {
  await mongoose.connection.dropDatabase();
  await mongoose.connection.close();
});

// use chaiHttp for making the actual HTTP requests
chai.use(chaiHttp);

describe('Dashboard API', function() {

  it('Adds example dashboard', function(done) {
    var MyTmpdashboard = new Dashboard({
      name: 'MyExampleDashboard',
      tangoDB: 'testdb',
      widgets: [],
      variables: [],
    });
    chai.request(server)
      .post('/dashboards/')
      .send(MyTmpdashboard)
      .end(function(err, res) {
        if (err) console.log(err);
        res.body.should.to.include({ created: true });
        done();
      });
  });

  it('should list ALL dashboards and query first one', function(done) {
    chai.request(server)
      .get('/dashboards/user/dashboards?tangoDB=testdb')
      .end(function(err, res) {
        if (err) console.log(err);
        if (res.body[0].id) {
          tmpDashID = res.body[0].id;
          chai.request(server)
            .get('/dashboards/' + res.body[0].id)
            .end(function(err, res) {
              if (err) console.log(err);
              res.status.should.be.equal(200);
              done();
            });
        }
        res.status.should.be.equal(200);
        done();
      });
  });

  it('should list group dashboardsCount', function(done) {
    chai.request(server)
      .get('/dashboards/group/dashboardsCount')
      .end(function(err, res) {
        if (err) console.log(err);
        res.status.should.be.equal(200);
        done();
      });
  });

  it('should miss group dashboards', function(done) {
    chai.request(server)
      .get('/dashboards/group/dashboards')
      .end(function(err, res) {
        if (err) console.log(err);
        res.status.should.be.equal(400);
        done();
      });
  });

  it('should list group dashboards', function(done) {
    chai.request(server)
      .get('/dashboards/group/dashboards?group=group1')
      .end(function(err, res) {
        if (err) console.log(err);
        res.status.should.be.equal(200);
        done();
      });
  });

  it('should share tmp dashboard', function(done) {
    var shareObj = { group: 'group2', groupWriteAccess: true};
    chai.request(server)
      .post('/dashboards/' + tmpDashID + '/share')
      .send(shareObj)
      .end(function(err, res) {
        if (err) console.log(err);
        res.status.should.be.equal(200);
        done();
      });
  });

  it('should clone tmp dashboard', function(done) {
    chai.request(server)
      .post('/dashboards/' + tmpDashID + '/clone')
      .end(function(err, res) {
        if (err) console.log(err);
        res.status.should.be.equal(200);
        done();
      });
  });

  it('should rename tmp dashboard', function(done) {
    var renameObj = { newName: 'newnameDash'};
    chai.request(server)
      .post('/dashboards/' + tmpDashID + '/rename')
      .send(renameObj)
      .end(function(err, res) {
        if (err) console.log(err);
        res.status.should.be.equal(200);
        done();
      });
  });

  it('should update example dashboard', function(done) {
    var MyTmpdashboard = {
      id: tmpDashID,
      name: 'MyExampleDashboardUp',
      tangoDB: 'testdb',
      widgets: [],
      variables: [],
    };
    chai.request(server)
      .post('/dashboards/')
      .send(MyTmpdashboard)
      .end(function(err, res) {
        if (err) console.log(err);
        res.body.should.to.include({ created: false });
        done();
      });
  });

  it('should delete tmp dashboard', function(done) {
    chai.request(server)
      .delete('/dashboards/' + tmpDashID)
      .end(function(err, res) {
        if (err) console.log(err);
        res.status.should.be.equal(200);
        res.body.should.to.include({ deleted: true });
        done();
      });
  });

});

Welcome to the Taranta dashboard-repo documentation!
====================================================

This project provides a cenntral store used by the Taranta suite for storing and sharing the dashboards created by taranta users.

It consists of:

- A data store (implemented as a  MongoDB database)
- A set of RESTful services used by the Taranta application to create, retrieve, update and delete individual dashboards associated with a particular user and access to one or more user groups.
- Basic action logging service / store for Taranta actions

The 'models' contains the data strucures used  to store the two types of information

.. toctree::
  :maxdepth: 2
  
  package/dashboardModel
  package/actionsModel
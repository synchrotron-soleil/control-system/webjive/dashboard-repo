*************************************
Data model for storing the dashboards 
*************************************

.. code-block:: javascript
  :linenos:

  let dashboardSchema = mongoose.Schema({
    _id: {
        type: ObjectId,
        auto: true,
    },
    name: {
        type: String,
        required: true,
    },
    user: {
        type: String,
        required: true,
    },
    widgets: {
        type: [WIDGET],
        required: true,
    },
    variables: {
        type: Object,
        required: false,
        default: [{}],
    },
    insertTime: {
        type: Date,
        default: Date.now,
    },
    updateTime: {
        type: Date,
        required: true,
        default: Date.now,
    },
    group: {
        type: String,
        required: false,
        default: null,
    },
    groupWriteAccess: {
        type: Boolean,
        required: false,
        default: false,
    },
    lastUpdatedBy: {
        type: String,
        required: false,
        default: null,
    },
    deleted: {
        type: Boolean,
        default: false,
    },
    tangoDB: {
        type: String,
        default: '',
    },
  });

.. code-block:: javascript
  :linenos:

  const WIDGET = {
    canvas: String,
    id: String,
    x: Number,
    y: Number,
    height: Number,
    width: Number,
    type: { type: String },
    // Necessary since `type' is a reserved word in Mongoose
    inputs: Schema.Types.Mixed,
    order: Number,
  };